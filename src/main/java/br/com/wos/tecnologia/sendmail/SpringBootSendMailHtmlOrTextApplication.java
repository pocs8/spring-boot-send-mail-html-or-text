package br.com.wos.tecnologia.sendmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSendMailHtmlOrTextApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSendMailHtmlOrTextApplication.class, args);
    }

}
