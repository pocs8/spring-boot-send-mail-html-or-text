package br.com.wos.tecnologia.sendmail.controller;

import br.com.wos.tecnologia.sendmail.model.Mail;
import br.com.wos.tecnologia.sendmail.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wesleysantos in 10/04/19
 */
@RestController
@RequestMapping("/mail")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MailController {

    private final MailService mailService;

    @PostMapping("/send-mail-text")
    public ResponseEntity<?> SendMailText(@RequestBody Mail mail) {

        mailService.SendMailText(mail);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/send-mail-html")
    public ResponseEntity<?> SendMailHTML(@RequestBody Mail mail) {

        mailService.SendMailHtml(mail);

        return ResponseEntity.ok().build();
    }
}
