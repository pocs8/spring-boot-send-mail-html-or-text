package br.com.wos.tecnologia.sendmail.service;

import br.com.wos.tecnologia.sendmail.model.Mail;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.util.Base64;


/**
 * Created by wesleysantos in 10/04/19
 */
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MailService {

    private final JavaMailSender mailSender;

    private final TemplateEngine templateEngine;

    @Value("${properties.mail}")
    private String sender;

    public void SendMailText(Mail mail) {

         MimeMessagePreparator messagePreparator = mimeMessage -> {
             MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
             messageHelper.setFrom(sender);
             messageHelper.setTo(mail.getRecipients().toArray(new String[mail.getRecipients().size()]));
             messageHelper.setSubject(mail.getSubject());
             messageHelper.setText(mail.getBody());

             setAttachments(mail, messageHelper);
         };

         try {
             mailSender.send(messagePreparator);
         } catch (MailException e) {
            log.error(e.getMessage(), e);
         }

    }

    public void SendMailHtml(Mail mail) {

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            messageHelper.setFrom(sender);
            messageHelper.setTo(mail.getRecipients().toArray(new String[mail.getRecipients().size()]));
            messageHelper.setSubject(mail.getSubject());
            messageHelper.setText(buildHtml(mail.getBody()), true);

            setAttachments(mail, messageHelper);
        };

        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            log.error(e.getMessage(), e);
        }

    }

    private String buildHtml(String message) {
        Context context = new Context();
        context.setVariable("message", message);
        return templateEngine.process("mailTemplate", context);
    }

    private void setAttachments(Mail mail, MimeMessageHelper messageHelper) {
        if (!mail.getAttachments().isEmpty()) {
            mail.getAttachments().forEach(attachment -> {
                try {
                    messageHelper.addAttachment(attachment.getFullName(), new ByteArrayResource(Base64.getDecoder().decode(attachment.getArchiveBase64())));
                } catch (MessagingException e) {
                    log.error(e.getMessage(), e);
                }
            });
        }
    }
}
