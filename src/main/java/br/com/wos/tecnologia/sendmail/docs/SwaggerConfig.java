package br.com.wos.tecnologia.sendmail.docs;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by wesleysantos in 10/04/19
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {
    public SwaggerConfig() {
        super("br.com.wos.tecnologia.sendmail.controller");
    }
}
