package br.com.wos.tecnologia.sendmail.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;

/**
 * Created by wesleysantos in 10/04/19
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Mail {

    private String subject;
    private List<String> recipients;
    private String body;
    private List<Attachment> attachments;
}
