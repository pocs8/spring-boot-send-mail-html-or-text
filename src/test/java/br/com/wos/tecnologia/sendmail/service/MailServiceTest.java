package br.com.wos.tecnologia.sendmail.service;

import br.com.wos.tecnologia.sendmail.model.Attachment;
import br.com.wos.tecnologia.sendmail.model.Mail;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Base64;

import static org.junit.Assert.*;

/**
 * Created by wesleysantos in 10/04/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class MailServiceTest {

    private GreenMail smtpServer;

    @Autowired
    private MailService mailService;

    @Before
    public void setUp() throws Exception {
        smtpServer = new GreenMail(new ServerSetup(2525, null, "smtp"));
        smtpServer.start();
    }

    @After
    public void tearDown() throws Exception {
        smtpServer.stop();
    }

    @Test
    public void sendMailText() throws Exception {
        Mail mail = new Mail("Teste - Envio E-mail", Arrays.asList("wesleyosantos91@gmail.com"), "Teste Unitário - Envio E-mail", Arrays.asList(new Attachment("teste.pdf", "c29tZSBzdHJpbmc=")));

        mailService.SendMailText(mail);
    }

    @Test
    public void SendMailHtml() throws Exception {
        Mail mail = new Mail("Teste - Envio E-mail", Arrays.asList("wesleyosantos91@gmail.com"), "Teste Unitário - Envio E-mail", null);

        mailService.SendMailHtml(mail);
    }

}