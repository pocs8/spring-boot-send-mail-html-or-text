package br.com.wos.tecnologia.sendmail;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class SpringBootSendMailHtmlOrTextApplicationTests {

    @Value("${properties.mail.test}")
    private String name;

    @Test
    public void contextLoads() {
        log.info(name);
    }

}
